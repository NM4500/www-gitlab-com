---
layout: markdown_page
title: "Travel and Expense Guidelines"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Travel and Expense Guidelines

### Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and summits.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-–-soda’s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

## Renting Cars in the United States and Canada

### Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the US or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that GitLab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

### Physical Damage – Collision Damage Waiver

**Do not** purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles.
 If travel to Mexico is required, **purchase** the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

## Renting Cars- Countries other than the US and Canada

### Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the US and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

### Physical Damage – Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the US and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.

### Personal Use by Employee or Family Members of Business Auto Rentals

Coverage is **not** provided for personal use of automobiles or when family members are driving. Please evaluate whether your own personal automobile insurance provides an extension for this coverage. If it does not, or you are renting a vehicle outside the US or Canada or taking a US rented vehicle into Mexico, we recommend that you purchase the liability and physical damage coverage offered by the rental agency to protect your personal liability when not engaged in company business. GitLab will not pay for liability or damage to the rental vehicle resulting from personal use or use by non-employees.


## Reimbursable Expenses

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and subsidiary assignment (Inc, LTD, BV, GmbH). Check with PeopleOps if you are unsure about either of these. 

For information regarding the company expense policy, check out the section of our team handbook on [Spending Company Money](/handbook/spending-company-money). The accounting team will review the expenses for compliance with the company travel policy.  Exceptions will be escalated to the manager for review and approval. The CEO will review selected escalations at least annually.

### Independent Contractors
Contributors who are legally classified as independent contractors should include reimbursable expenses on the invoices they submit to the company through our Accounts Payable mailbox (ap@gitlab.com). These team members should also consider the terms and conditions of their respective contractor agreements, when submitting invoices to the company.  

### Employees
Contributors who are on GitLab’s payroll are legally classified as employees. These team members will submit expenses in the Expensify application. Expense reports are to be submitted once a month, at least. If you were not assigned a user license as part of the onboarding process, you can request one by contacting PeopleOps. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here.](https://docs.expensify.com/using-expensify-day-to-day/using-expensify-as-an-expense-submitter/report-actions-create-submit-and-close)

## Non-Reimbursable Expenses

In order to purchase goods and services on behalf of the company, you should first [consult the Signature Authorization Matrix](/handbook/finance/authorization-matrix/) to determine the approval requirements. Note that this **does not** include travel expenses and other incidentals. These expenses should be self-funded then submitted for reimbursement within Expensify, or in the case of independent contractors, included in invoices to the company (per the guidelines above). 

If approval is not required, then proceed and send the invoice to Accounts Payable (ap@gitlab.com). If approval is required, create a confidential issue in the [Finance Issue Tracker](https://gitlab.com/gitlab-com/finance) and tag the required functional and financial approvers. Most importantly, the team member making the purchase request is ultimately responsible for final review and approval of the invoices. Final review and approval are critical process controls that help ensure we don't make erroneous payments to vendors. All original invoices and payment receipts must be sent to Accounts Payable.

Examples of things we have not reimbursed:
1. Costume for end of summit party.
1. Boarding expense for dog while traveling.
1. Headphones costing $800 which were found to be in excess of our standard equipment guidelines.
1. Batteries for smoke detector.
1. Meals during the summit when team members opt out of the company provided meal option.
1. Cellphones and accessories. 

## Marketing Campaign Expenses
Please see the [campaign expense guidelines in the Marketing handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/marketing-operations/#campaign-cost-tracking).

## Company Credit Cards
<a name="company-cc"></a>

We have worked to reduce the number of outstanding company credit cards in an effort to centralize corporate purchasing, however, there are still certain situations where it may be more practical to issue additional corporate cards. This will be addressed on a case-by-case basis and final approval will come from the CFO. Please contact the Finance team if you would like further information on this. 

GitLabbers carrying company cards in their name are required to submit expenses in [Expensify](https://www.expensify.com/) on a monthly basis. Reports are to be submitted no later than the fourth business day of each month as the American Express statement period-end date is the 28th of each month. 

Card transactions are auto-imported into Expensify which then auto-generates the expense reports to match that of the American Express statements. Below you will find directions on how to sign up for an American Express account and submit these expenses in accordance with company policy.

### Creating an American Express account

Once you receive your card, [register the card in the American Express portal.](http://www.americanexpress.com/confirmcard)

### Submitting company credit card expenses

On the 30th of each month, Expensify will auto-generate your expense report and send an email notification when the report is ready, leaving 4 main steps to prepare your report for submission: 

#### 1. Coding expenses
- In some cases, Expensify reports will show payment processor names (i.e. Stripe, PayPal) rather than the actual payee merchant, making it difficult to identify charges. This can be resolved by downloading your Amex statement from the portal, which contains greater detail.


#### 2. Required supplementary information
Certain purchases require additional data in order for the Finance team to accurately process transactions. 

Expense reports are considered incomplete if missing any of the following data:
- **Expense Tags:** If applicable, add expense tags. Common tag examples include company summits, marketing campaigns, and professional service engagements.
- **Laptops/Equipment:** Purchases of laptops and other assets in excess of $1,000 USD (per item) must include the name of the GitLabber for whom the asset was purchased. 
- **Airfare/Travel:** Purchases of flights, transportation, and lodging on behalf of other GitLabbers must also include the name of the corresponding individual.

#### 3. Attaching receipts
- Any expense over $25 USD requires a legible receipt. No exceptions.

#### 4. Submit the report
- Reports are due on the 4th business day of each month. Failure to meet these policy guidelines on an ongoing basis will result in permanent cancellation of your card.  

 **Note that company credit card expenses should never be commingled with reimbursable expense reports. Reimbursable expenses should always be submitted separately. Please contact the Accounting Manager if you have any questions.**   


