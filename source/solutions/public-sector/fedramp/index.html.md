---
layout: markdown_page
title: "FedRAMP and GitLab"
---

## What is FedRAMP?

The Federal Risk and Authorization Management Program (FedRAMP) is a government-wide program that provides a standardized approach to security assessment, authorization, and continuous monitoring for cloud products and services. [From the GSA definition](https://www.gsa.gov/technology/government-it-initiatives/fedramp)

Specifically, FedRAMP is focused on cloud products and services, evaluating and certifying cloud PAAS, IAAS and SAAS offerings.   Details about the [FedRAMP program](https://www.fedramp.gov/) highlight the process and status of how cloud services are assessed and certified in the [FedRAMP marketplace.](https://marketplace.fedramp.gov/#/products?sort=productName)

## GitLab and FedRAMP

GitLab is **both** a product that you can host and a cloud service that we host.

You can deploy GitLab into your FedRAMP cloud PaaS such as [AWS](https://docs.gitlab.com/ee/install/aws/), [Google Cloud](/ee/install/google_cloud_platform/index.html), [Azure](https://docs.gitlab.com/ee/install/azure/index.html) and others.   In fact, many agencies and customers deploy and manage their own instance of GitLab in their cloud.

For our hosted offering of GitLab.com, we're working toward FedRAMP certification.

If you want to learn more about GitLab and how we support public sector agencies, departments and organizations, please contact us.

<center><a href="/solutions/public-sector/#contact" class="btn cta-btn orange">contact our public sector team</a></center>
